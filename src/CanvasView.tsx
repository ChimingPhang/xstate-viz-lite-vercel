import React, { useEffect, useMemo, useState } from 'react'

import { AddIcon, MinusIcon, RepeatIcon } from '@chakra-ui/icons'
import {
  Box,
  Button,
  ButtonGroup,
  IconButton,
  Spinner,
  VStack
} from '@chakra-ui/react'
import { useSelector } from '@xstate/react'
import { createMachine, State } from 'xstate'

import { CanvasContainer } from './CanvasContainer'
import { useCanvas } from './CanvasContext'
import { canZoom, canZoomIn, canZoomOut } from './canvasMachine'
import { DirectedGraphNode, toDirectedGraph } from './directedGraph'
import { useEmbed } from './embedContext'
import { Graph } from './Graph'
import { CompressIcon, HandIcon } from './Icons'
import { Overlay } from './Overlay'
import { useSimulation, useSimulationMode } from './SimulationContext'
import {
  getInitialMachineCode,
  getInitialMachineState,
  useSourceActor
} from './sourceMachine'

export const CanvasView: React.FC = () => {
  // TODO: refactor this so an event can be explicitly sent to a machine
  // it isn't straightforward to do at the moment cause the target machine lives in a child component
  const [panModeEnabled, setPanModeEnabled] = React.useState(false);
  const embed = useEmbed();
  const simService = useSimulation();
  const canvasService = useCanvas();
  const [sourceState] = useSourceActor();
  const machine = useSelector(simService, (state) => {
    return state.context.currentSessionId
      ? state.context.serviceDataMap[state.context.currentSessionId!]?.machine
      : undefined;
  });

  const isLayoutPending = useSelector(simService, (state) =>
    state.hasTag('layoutPending'),
  );
  const isEmpty = useSelector(simService, (state) => state.hasTag('empty'));
  const [digraph, setDigraph] = useState<DirectedGraphNode>()

  const shouldEnableZoomOutButton = useSelector(
    canvasService,
    (state) => canZoom(embed) && canZoomOut(state.context),
  );

  const shouldEnableZoomInButton = useSelector(
    canvasService,
    (state) => canZoom(embed) && canZoomIn(state.context),
  );

  const simulationMode = useSimulationMode();

  const canShowWelcomeMessage = sourceState.hasTag('canShowWelcomeMessage');

  const showControls = useMemo(
    () => !embed?.isEmbedded || embed.controls,
    [embed],
  );

  const showZoomButtonsInEmbed = useMemo(
    () => !embed?.isEmbedded || (embed.controls && embed.zoom),
    [embed],
  );
  const showPanButtonInEmbed = useMemo(
    () => !embed?.isEmbedded || (embed.controls && embed.pan),
    [embed],
  );

  useEffect(() => {
    setDigraph(machine ? toDirectedGraph(machine) : undefined)
  }, [machine])

  const getInitialMachineAndState = async () => {
    const machineObj = await getInitialMachineCode()
    const initialState = await getInitialMachineState()
    simService.send({
      type: 'MACHINES.REGISTER',
      machines: [createMachine(machineObj)],
      initialState
    });
  }
  useEffect(() => {
    getInitialMachineAndState()
  }, [])

  return (
    <Box
      display="grid"
      height="100%"
    >

      <CanvasContainer panModeEnabled={panModeEnabled}>
        {digraph && <Graph digraph={digraph} />}
        {(isEmpty && canShowWelcomeMessage) || isLayoutPending && (
          <Overlay>
            <Box textAlign="center">
              <VStack spacing="4">
                <Spinner size="xl" />
                <Box>Visualizing machine...</Box>
              </VStack>
            </Box>
          </Overlay>
        )}
      </CanvasContainer>

      {showControls && (
        <Box
          display="flex"
          flexDirection="row"
          alignItems="center"
          justifyContent="flex-start"
          position="absolute"
          bottom={0}
          left={0}
          paddingX={2}
          paddingY={3}
          zIndex={1}
          width="100%"
          data-testid="controls"
        >
          <ButtonGroup size="sm" spacing={2} isAttached>
            {showZoomButtonsInEmbed && (
              <>
                <IconButton
                  aria-label="Zoom out"
                  title="Zoom out"
                  icon={<MinusIcon />}
                  disabled={!shouldEnableZoomOutButton}
                  onClick={() => canvasService.send('ZOOM.OUT')}
                  variant="secondary"
                />
                <IconButton
                  aria-label="Zoom in"
                  title="Zoom in"
                  icon={<AddIcon />}
                  disabled={!shouldEnableZoomInButton}
                  onClick={() => canvasService.send('ZOOM.IN')}
                  variant="secondary"
                />
              </>
            )}
            <IconButton
              aria-label="Fit to content"
              title="Fit to content"
              icon={<CompressIcon />}
              onClick={() => canvasService.send('FIT_TO_CONTENT')}
              variant="secondary"
            />
            <IconButton
              aria-label="Reset canvas"
              title="Reset canvas"
              icon={<RepeatIcon />}
              onClick={() => canvasService.send('POSITION.RESET')}
              variant="secondary"
            />
          </ButtonGroup>
          {showPanButtonInEmbed && (
            <IconButton
              aria-label="Pan mode"
              icon={<HandIcon />}
              size="sm"
              marginLeft={2}
              onClick={() => setPanModeEnabled((v) => !v)}
              aria-pressed={panModeEnabled}
              variant={panModeEnabled ? 'secondaryPressed' : 'secondary'}
            />
          )}
          {simulationMode === 'visualizing' && (
            <Button
              size="sm"
              marginLeft={2}
              onClick={() => simService.send('MACHINES.RESET')}
              variant="secondary"
            >
              RESET
            </Button>
          )}
        </Box>
      )}
    </Box>
  );
};
